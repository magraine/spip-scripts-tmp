# Usage
# Dans un répertoire, avoir 2 spip côte à côte :
# checkout spip -bmaster git@git.spip.net dev
# checkout spip -b4.2 git@git.spip.net 42
# sh diff-lang.sh dev 42

from=$1
to=$2

prompt() {
    echo
    read -p "$1"
    echo
}

verif_one() {
    _from=$1
    _to=$2
    _lang=$3
    if [ -d $_to ]; then 
        printf '%s ' $_to;
        if ! diff -q -r -b -w $_from$_lang/lang $_to$_lang/lang &>/dev/null; then
            # need lang update
            printf '⬆ ';
        else
            printf '✅ ';
        fi
        old=`pwd`
        cd $_to;
        if [[ `git status --porcelain` ]]; then
            echo '❌';
            git diff;
            git st;
            echo
        else
            echo '✅';
        fi
        cd $old;
    fi;
}

verif_all() {
    echo "Vérifs des diffs"
    verif_one "$from" "$to" "/ecrire"
    for i in $(ls $to/plugins-dist); do 
        verif_one "$from/plugins-dist/$i" "$to/plugins-dist/$i"
    done;
}

changelog() {
    _section=$1
    _type=$2
    _message=$3
    echo
    echo "## $1"
    echo
    echo "### $2"
    echo
    echo "- $3"
    echo
}

lang_one() {
    _from=$1
    _to=$2
    _lang=$3
    if [ -d $_to ]; then 
        printf '%s ' $_to;
        if ! diff -q -r -b -w $_from$_lang/lang $_to$_lang/lang &>/dev/null; then
            echo '⬆';
            cp -r $_from$_lang/lang/* $_to$_lang/lang/
            old=`pwd`
            cd $_to;
            if ! [[ -z "$(git status --porcelain)" ]]; then
                echo $i;
                git diff;
                git st;
                prompt "C’est bon ? (commit + proposera changelog)"
                git add .
                git commit -m "i18n: Report chaines de langues (Salvatore)"
                changelog "Unreleased" "Changed" "Mise à jour des chaînes de langues depuis trad.spip.net"
                echo "▶ Éditer le fichier $_to/CHANGELOG.md à la main"
                prompt "C’est fait ? (commitera le changelog)"
                git add CHANGELOG.md
                git commit -m "docs(changelog): i18n"
                prompt "C’est bon ? (dry-run)"
                git pull
                git push --dry-run
                prompt "C’est bon ? (fera push)"
                git push
            fi;
            cd $old;
        else 
            echo '✅'
        fi
    fi
}

lang_all() {
    echo "Applique les langues"
    lang_one "$from" "$to" "/ecrire"
    for i in $(ls $to/plugins-dist); do 
        lang_one "$from/plugins-dist/$i" "$to/plugins-dist/$i"
    done;
}

verif_all
prompt "Continuer ?"
lang_all